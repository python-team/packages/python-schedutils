Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: python-schedutils
Upstream-Contact: Jiri Kastner <jkastner@redhat.com>
Source: https://git.kernel.org/pub/scm/libs/python/python-schedutils/python-schedutils.git/

Files: *
Copyright: 2008 Red Hat Inc. <jkastner@redhat.com>
License: GPL-2
Comment: Content was authored by Arnaldo Carvalho de Melo <acme@redhat.com> in
 2008. Maintainership transferred to Jiri Kastner <jkastner@redhat.com> in
 2015. Jiri Kasterner (as of 2018) is the point of contact for all upstream
 matters related to this library. This copyright information was derived from
 files pchrt and ptaskset, two applications distributed with this library.

Files: debian/*
Copyright: 2018 Stewart Ferguson <stew@ferg.aero>
License: GPL-2
Comment: This package was debianized by Stewart Ferguson <stew@ferg.aero> in
 November 2018.  The current debian maintainer is Stewart Ferguson.

Files: python-schedutils/py3compat.h
Copyright: 2015 Petr Viktorin <pviktori@redhat.com>
License: GPL-2

Files: python-schedutils/schedutils.c
Copyright: 2004 Silicon Graphics, Inc. (SGI)
License: GPL-2
Comment: This copyright claim only affects a portion of this file. The claim
 specifically targets bitmask declarations, bitmask_*() routines, and
 associated *_setbit() and _getbit() routines.

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
